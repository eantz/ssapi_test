def config():
    return {
        'base_url' : 'http://localhost:5000/',
        'category_id': 8,
        'false_category_id': 'lala',
        'data_insert_category_wo_parent': {
            'name':'Hat'
        },
        'data_insert_category_w_parent': {
            'parent_id':10,
            'name':'Glove'
        },
        'category_id_to_update_wo_parent': 8,
        'data_update_category_wo_parent': {
            'name':'Modern Gown'
        },
        'category_id_to_update_w_parent': 9,
        'data_update_category_w_parent': {
            'parent_id':8,
            'name':'Hand Glove'
        },
        'category_id_to_delete':11,
        'product_id':1,
        'false_product_id':'baba',
        'data_insert_product': {
            'category_id':3,
            'sku':'aaabbb',
            'name':'Sunglass Branded',
            'color':'silver',
            'size':'S',
            'price':'80000'
        },
        'product_id_to_update':4,
        'data_update_product': {
            'category_id':3,
            'sku':'aaabbbfff',
            'name':'Hot Pants',
            'color':'pink',
            'size':'S',
            'price':'120000'
        },
        'product_id_to_delete':6
    }

def extract_message(message_obj):
    if 'message' in message_obj:
        return message_obj['message']
    else:
        return ''