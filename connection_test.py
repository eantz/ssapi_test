import unittest
import requests
import config

class ConnectionTest(unittest.TestCase):
    def test_connection(self):
        c = config.config()
        r = requests.get(c['base_url']);
        self.assertEqual(r.status_code, 200, 'connection failed');