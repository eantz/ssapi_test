import unittest
import requests
import config

class CategoriesTest(unittest.TestCase):
    def test_category(self):
        c = config.config()
        category_id = c['category_id']
        r = requests.get(c['base_url'] + 'category/' + str(category_id))
        result = r.json();
        message = config.extract_message(result)
        self.assertEqual(int(result['id']), category_id, message)

    @unittest.expectedFailure
    def test_category_fail(self):
        c = config.config()
        category_id = c['false_category_id']
        r = requests.get(c['base_url'] + 'category/' + str(category_id))
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(r.status_code, 200, message);

    def test_get_all_category(self):
        c = config.config()
        r = requests.get(c['base_url'] + 'category')
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(r.status_code, 200, message);

    def test_insert_category_wo_parent(self):
        c = config.config()
        params = c['data_insert_category_wo_parent']
        r = requests.post(c['base_url'] + 'category', data=params)
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(result['name'], params['name'], message)

    def test_insert_category_w_parent(self):
        c = config.config()
        params = c['data_insert_category_w_parent']
        r = requests.post(c['base_url'] + 'category', data=params)
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(result['parent_id'], params['parent_id'], message)

    def test_update_category_wo_parent(self):
        c = config.config()
        params = c['data_update_category_wo_parent']
        r = requests.put(c['base_url'] + 'category/' + str(c['category_id_to_update_wo_parent']), data=params)
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(result['name'], params['name'], message)

    def test_update_category_w_parent(self):
        c = config.config()
        params = c['data_update_category_w_parent']
        r = requests.put(c['base_url'] + 'category/' + str(c['category_id_to_update_w_parent']), data=params)
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(result['parent_id'], params['parent_id'], message)

    def test_delete_category(self):
        c = config.config()
        r = requests.delete(c['base_url'] + 'category/' + str(c['category_id_to_delete']))
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(r.status_code, 200, message);