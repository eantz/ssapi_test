import unittest
import requests
import config

class ProductsTest(unittest.TestCase):
    def test_product(self):
        c = config.config()
        product_id = c['product_id']
        r = requests.get(c['base_url'] + 'product/' + str(product_id))
        result = r.json();
        message = config.extract_message(result)
        self.assertEqual(int(result['id']), product_id, message)

    @unittest.expectedFailure
    def test_product_fail(self):
        c = config.config()
        product_id = c['false_product_id']
        r = requests.get(c['base_url'] + 'product/' + str(product_id))
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(r.status_code, 200, message);

    def test_get_all_product(self):
        c = config.config()
        r = requests.get(c['base_url'] + 'product')
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(r.status_code, 200, message);

    def test_insert_product(self):
        c = config.config()
        params = c['data_insert_product']
        r = requests.post(c['base_url'] + 'product', data=params)
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(result['name'], params['name'], message)

    def test_update_product(self):
        c = config.config()
        params = c['data_update_product']
        r = requests.put(c['base_url'] + 'product/' + str(c['product_id_to_update']), data=params)
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(result['name'], params['name'], message)

    def test_delete_product(self):
        c = config.config()
        r = requests.delete(c['base_url'] + 'product/' + str(c['category_id_to_delete']))
        result = r.json()
        message = config.extract_message(result)
        self.assertEqual(r.status_code, 200, message);