import unittest
from connection_test import ConnectionTest
from categories_test import CategoriesTest
from products_test import ProductsTest

if __name__ == '__main__':
    unittest.main()